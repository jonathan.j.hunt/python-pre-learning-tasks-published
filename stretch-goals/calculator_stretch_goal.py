def calculator(a, b, operator):
    # ==============
    if operator == "+":
        x = (a + b)
        y = int(x)
        answer = bin(y)
        return answer[2:]

    elif operator == "-":
        x = (a - b)
        y = int(x)
        answer = bin(y)
        return answer[2:]

    elif operator == "*":
        x = (a * b)
        y = int(x)
        answer = bin(y)
        return answer[2:]

    elif operator == "/":
        x = (a / b)
        y = int(x)
        answer = bin(y)
        return answer[2:]

    else:
        print("Please enter a valid calculator function.")

    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
