def vowel_swapper(string):
    # ==============
    step1 = string.replace("a", "4")
    step2 = step1.replace("e", "3")
    step3 = step2.replace("i", "!")
    step4 = step3.replace("o", "ooo")
    step5 = step4.replace("O", "000")
    step6 = step5.replace("u", "|_|")
    step7 = step6.replace("A", "4")
    step8 = step7.replace("E", "3")
    step9 = step8.replace("I", "!")
    step10 = step9.replace("U", "|_|")

    return step10
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console